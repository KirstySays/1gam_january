﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class AddStroke : MonoBehaviour {

    private Text text;

    void OnEnable()
    {
        BallControl.HitBall += IncreaseStroke;
        BallControl.NextHole += Reset;
    }

    void OnDisable()
    {
        BallControl.HitBall -= IncreaseStroke;
        BallControl.NextHole -= Reset;
    }

    private void Start()
    {
        text = GetComponent<Text>();
    }
    void IncreaseStroke()
    {
        text.text += "I";
    }

    void Reset()
    {
        text.text = "";
    }
}
