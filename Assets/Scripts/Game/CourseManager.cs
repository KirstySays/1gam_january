﻿using UnityEngine;
using System.Collections;

public class CourseManager : MonoBehaviour
{
    public GameObject[] courses;
    public Color[] backgroundColours;
    private int currentHole;
    private bool changingColour;

    public delegate void Event();
    public static event Event FinishedCourses;

    void OnEnable()
    {
        BallControl.NextHole += NextCourse;
    }

    void OnDisable()
    {
        BallControl.NextHole -= NextCourse;   
    }

    // Use this for initialization
    void Start()
    {
        currentHole = -1;
    }

    private void Update()
    {
        if(changingColour)
        {
            Camera.main.backgroundColor = Color.Lerp(Camera.main.backgroundColor, backgroundColours[currentHole], 2.5f * Time.deltaTime);

            if (Camera.main.backgroundColor == backgroundColours[currentHole])
                changingColour = false;
        }
        
    }
    void NextCourse()
    {
        if (currentHole != -1)
            courses[currentHole].gameObject.SetActive(false);

        currentHole++;

        if (currentHole < courses.Length)
        {
            courses[currentHole].gameObject.SetActive(true);
            changingColour = true;
        }
        else
            FinishedCourses();
    }

}
