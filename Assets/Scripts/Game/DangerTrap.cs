﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class DangerTrap : MonoBehaviour
    {
        public delegate void Event();
        public static event Event HitTrap;


        void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Ball")
                HitTrap();
              
        }
    }
}

