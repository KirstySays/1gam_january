﻿using UnityEngine;
using System.Collections;

public class EndGameButtons : MonoBehaviour
{
    public delegate void Event();
    public static event Event Reset;

    public void Restart()
    {
        Reset();
    }

    public void Quit()
    {
        Application.Quit();
    }
}
