﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

using SceneManagement = UnityEngine.SceneManagement;

namespace Generic
{
    public class SceneManager : MonoBehaviour
    {
        void Awake()
        {
            DontDestroyOnLoad(transform.gameObject);
        }

        void OnEnable()
        {
            EndGameButtons.Reset += Restart;
            CourseManager.FinishedCourses += ShowEndGameScreen;
        }

        void OnDisable()
        {
            EndGameButtons.Reset -= Restart;
            CourseManager.FinishedCourses -= ShowEndGameScreen;
        }

        void ShowEndGameScreen()
        {
            SceneManagement.SceneManager.LoadScene("EndGame");
        }

        void Restart()
        {
            SceneManagement.SceneManager.LoadScene("Main");
        }
    }

}
