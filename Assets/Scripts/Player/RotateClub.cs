﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateClub : MonoBehaviour {

    private Vector3 previousMousePosition;

	// Use this for initialization
	void Start () {
        previousMousePosition = Vector3.zero;
	}


    bool HasMouseMoved()
    { 
        return (Input.GetAxis("Mouse X") != 0) || (Input.GetAxis("Mouse Y") != 0);
    }

    // Update is called once per frame
    void Update () {

        Vector3 mousePos = Input.mousePosition;

        //mousePos.z = 5.23f;

        if (HasMouseMoved())               //Mouse position will override the arrows
        {
            Vector3 objectPos = Camera.main.WorldToScreenPoint(transform.position);
            mousePos.x = mousePos.x - objectPos.x;
            mousePos.y = mousePos.y - objectPos.y;
            float angle = Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(new Vector3(90, 0, angle -90));
        }
        else
        {

            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
                transform.Rotate(0, 0, 1);
         
 
            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
                transform.Rotate(0, 0, -1);
        }
    }
}
