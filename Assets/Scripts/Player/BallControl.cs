﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallControl : MonoBehaviour
{

     private Rigidbody rigidbody = new Rigidbody();
    [SerializeField] private GameObject pivotPoint;
    [SerializeField] private Slider powerBarUI;
    [SerializeField] private Transform startingPosition;
    [SerializeField] private AudioClip ballHit;
    [SerializeField] private AudioClip reachedGoal;
    [SerializeField] private AudioClip[] boundryHit;
    [SerializeField] private GameObject collisionPrefab;

    private AudioSource audioSource;

    private float maximumPower, power;
    [SerializeField] private float baseSpeed = 5f;
    private float minimumScale = 0.6f;
    private float maximumScale = 1.3f;
    private ParticleSystem ballHitParticle;
    private Vector3 previousPosition;

    bool hasReset, isMoving, isPoweringUp, hasMoved;
    enum BallState { MOVING, POTTED, STARTPOS, WAITINGSHOT, POWERINGUP};
    BallState currentBallState;

    public delegate void Event();
    public static event Event NextHole;
    public static event Event HitBall;

    // Use this for initialization
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        ballHitParticle = GetComponent<ParticleSystem>();
        hasReset = isPoweringUp = hasMoved = false;
        isMoving = false;
        previousPosition = Vector3.zero;
        currentBallState = BallState.STARTPOS;

        maximumPower = 100;
        power = 0;

        //Set to the starting position if not already there
        ResetBall();
    }

    void OnEnable()
    {
        Game.DangerTrap.HitTrap += ResetBall;
    }

    void OnDisable()
    {
        Game.DangerTrap.HitTrap -= ResetBall;
    }

    private bool InputCheckDown(string inputControl)
    {
        if (inputControl == "power")
        {
            if (Input.GetButtonDown("Fire1") || Input.GetKeyDown(KeyCode.Space))
                return true;
            else
                return false;
        }
        else
        {
            Debug.LogError("Unable to find the input control " + inputControl);
            return false;
        }
    }

    private bool InputCheckUp(string inputControl)
    {
        if (inputControl == "power")
        {
            if (Input.GetButtonUp("Fire1") || Input.GetKeyUp(KeyCode.Space))
                return true;
            else
                return false;
        }
        else
        {
            Debug.LogError("Unable to find the input control " + inputControl);
            return false;
        }
    }


    // Update is called once per frame
    void Update()
    {
        BallState previousBallState = currentBallState;

        if (InputCheckDown("power") && (currentBallState == BallState.WAITINGSHOT || currentBallState == BallState.STARTPOS))
            currentBallState = BallState.POWERINGUP;          
        else if (rigidbody.velocity.magnitude < 0.35f && currentBallState == BallState.MOVING)          
            currentBallState = BallState.WAITINGSHOT;


        if ((InputCheckUp("power") && currentBallState == BallState.POWERINGUP) || power >= maximumPower)
        {
            //Fire the ball 
            audioSource.PlayOneShot(ballHit);
            HitBall();
            ballHitParticle.Play();
         
            StartCoroutine(WaitBeforeSettingMoving(0.1f));          // Don't set this too high otherwise we can introduce the bug where the user can spam the mouse button. 

           // currentBallState = BallState.MOVING;
            rigidbody.AddRelativeForce(0, 0, baseSpeed * power);
            power = 0;
        }
   
        if (currentBallState == BallState.POWERINGUP && power < maximumPower)
        {
            //Charge up the power bar
            power++;
            powerBarUI.value = power;
            pivotPoint.transform.localScale += new Vector3(0, 0.01f, 0);
     
        }

        if(currentBallState != BallState.POTTED || currentBallState != BallState.MOVING)
        {
            Vector3 mousePos = Input.mousePosition;
            mousePos.z = 5.23f;

            if (HasMouseMoved())               //Mouse position will override the arrows
            {
                Vector3 objectPos = Camera.main.WorldToScreenPoint(transform.position);
                mousePos.x = mousePos.x - objectPos.x;
                mousePos.y = mousePos.y - objectPos.y;
                float angle = Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg;
                transform.rotation = Quaternion.Euler(new Vector3(0, -(angle - 90), 0));
            }
            else
            {

                if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
                    transform.Rotate(0, -1, 0);


                if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
                    transform.Rotate(0, 1, 0);
            }

        }

        if (previousBallState != currentBallState)
            handleChangeOfBallState();
    }

    void handleChangeOfBallState()
    {
        switch(currentBallState)
        {
            case BallState.STARTPOS:
                break;
            case BallState.MOVING:
                  isPoweringUp = false;

                

                hasReset = false;
                isMoving = true;
               
                power = 0;
                powerBarUI.value = power;

                break;
            case BallState.WAITINGSHOT:
                rigidbody.velocity = Vector3.zero;
                rigidbody.angularVelocity = Vector3.zero;
                isMoving = false;
                power = 0;
                powerBarUI.value = power;
                pivotPoint.transform.localScale = new Vector3(0.375f, minimumScale, 0.375f);
                pivotPoint.transform.position = transform.position;
                pivotPoint.SetActive(true);
                break;
            case BallState.POWERINGUP:
                isPoweringUp = true;
                audioSource.Play();
                break;
            case BallState.POTTED:
                ResetBall();
                NextHole();
                break;
        }
    }

    bool HasMouseMoved()
    {
        return (Input.GetAxis("Mouse X") != 0) || (Input.GetAxis("Mouse Y") != 0);
    }

    public void ResetBall()
    {
        rigidbody.velocity = Vector3.zero;
        rigidbody.angularVelocity = Vector3.zero;
        transform.position = startingPosition.position;
        isMoving = false;
        pivotPoint.transform.position = transform.position;
        pivotPoint.SetActive(true);
        hasReset = true;
        power = 0;
        pivotPoint.transform.localScale = new Vector3(0.375f, minimumScale, 0.375f);
        isPoweringUp = false;
        currentBallState = BallState.STARTPOS;
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Goal")
        {
            if (other.gameObject.GetComponent<ParticleSystem>())
                other.gameObject.GetComponent<ParticleSystem>().Play();

            audioSource.PlayOneShot(reachedGoal);
            currentBallState = BallState.POTTED;
            handleChangeOfBallState();
           
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Boundry")
        {
            audioSource.PlayOneShot(boundryHit[Random.Range(0, boundryHit.Length-1)]);
            collision.gameObject.GetComponent<ShakeObject>().Shake();
        }
    }

    private IEnumerator WaitBeforeSettingMoving(float waitTime)
    {
        pivotPoint.SetActive(false);
        yield return new WaitForSeconds(waitTime);
        currentBallState = BallState.MOVING;
    }
}


    