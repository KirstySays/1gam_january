﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Putter : MonoBehaviour {


    private ConstantForce force;

	// Use this for initialization
	void Start ()
    {
        force = GetComponent<ConstantForce>();
        force.enabled = false;
        
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (Input.GetButtonDown("Fire1"))
        {
            force.enabled = true;
        }

    }

    void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);
    }
}
