﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace GameTweening
{
    public class ScaleUpTween : MonoBehaviour
    {
        private bool hasScaled = false;         //temp variable so we can play this animation only once

        [SerializeField] private Vector3 startingScale;
        [SerializeField] private Vector3 scaleAmount;              //What the animation will go to first before calming down to the original scale.
        //[SerializeField] private Vector3 punchAmount;
        [SerializeField] private float duration;
        // [SerializeField] private int vibrato;               //How much will the punch vibrate
        //[SerializeField][Range (0,1)] private float elasticity;          // range between 0 -1 on how much the vector will go beyond the starting scale when bouncing backwards

        AudioSource audioSource;

        private Vector3 originalScale;
        private Vector3 maximumScale;

        // Use this for initialization
        void Start()
        {
            originalScale = transform.localScale;
            transform.localScale = startingScale;
            maximumScale = originalScale + scaleAmount;

            audioSource = GetComponent<AudioSource>();
        }

        // Update is called once per frame
        void Update()
        {
            // when we've been made active, then this course is being rendered. 

            // play the sound while we : 

            // do some animation
            if (!hasScaled)
            {
          
                Sequence scaleSequence = DOTween.Sequence();
                scaleSequence
                .Append(transform.DOScale(originalScale, duration))
                .PlayForward();

                if(audioSource)
                    audioSource.Play();
                hasScaled = true;
            }
            // let the game know that it can start accepting input from the user
        }
    }

}
