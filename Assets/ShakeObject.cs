﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ShakeObject : MonoBehaviour {

    [SerializeField] private float speed = 1.0f; //how fast it shakes
    [SerializeField] private float amount = 1.0f; //how much it shakes

    public void Shake()
    {
        //transform.DOShakeRotation(1, 25, 10, 2, true);
        if(!DOTween.IsTweening(transform))
            transform.DOPunchRotation(Vector3.forward, 1, 4, 1);
    }
}

